package ua.fintech.monokino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonoKinoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonoKinoApplication.class, args);
	}

}
